# aws_creds

Post-exploitation Metasploit module that gathers AWS access keys for all users on a compromised system.

### Requirements

Metasploit

### Installation
1. mkdir -p ~/.msf4/modules/post/multi/gather
2. git clone https://github.com/astlawrence/aws_creds.git
3. cp aws_creds/aws_creds.rb ~/.msf4/modules/post/multi/gather/

### Usage
```
use post/multi/gather/aws_creds
set session #
run
```
